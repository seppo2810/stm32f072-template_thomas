# stm32f072b demo

## Prerequisites
The project depends on several external tools for building:

- Rake (via Ruby 2.0)
- GCC ARM toolchain

Furthermore these tools are proposed to be able to develop and debug the application:
- Eclipse
- OpenOCD
- [ST-Link USB driver](http://www.st.com/web/catalog/tools/FM146/CL1984/SC724/SS1677/PF251168?sc=internet/evalboard/product/251168.jsp)


The tools should be installed directly into the following locations with exception of the ST-Link USB driver, which can be installed in any location:

Ruby 2.0:
C:\Ruby200-x64

GCC arm toolchain:
C:\Program Files (x86)\GNU Tools ARM Embedded\4.8 2013q4

OpenOCD:
C:\Program Files\openocd-0.8.0\bin


## Initial import into eclipse
The project can be imported into an eclipse workspace by navigating to
"File->Import->Existing Project into Workspace" and choosing the directory of the project.

## Building from Eclipse
The Eclipse project is already configured to build all required resources if one uses "Build Project" by right clicking on the project in the project explorer.
Further targets are configured in the 'Make Target' view of Eclipse.

## Debugging
The Eclipse project has already preconfigured debug configurations for flashing the ioc executable on the target and to single step debug on the target directly.
Beforehand OpenOCD has to be executed. It can be started via 'Run->External Tools->openocd-0.7.0'. After executing, OpenOCD will run in the background and is ready
to accept debugging connections.

## Building from command line
It's important to have the following build tools in your PATH variable:

* ruby binary directory (test by executing ruby -v)

Furthermore you should have a corresponding environment file that fits to the path of the installed ARM GCC toolchain.
The environment file that normally is used, is rake/toolchain/environment-arm-none-eabi. This can be used as a template and adapted according to the toolchain location.

The command to execute a build on the commandline is:

    rake.bat all TOOLCHAIN_ENV=<Path to environment file> RELEASE=1

With 'rake.bat -T' a list of possible targets/tasks can be printed out on the command line.
